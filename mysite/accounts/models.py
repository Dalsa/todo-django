from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

import uuid

class UserManager(BaseUserManager):
    def create_user(self, email, name, nickname, phone, password=None):
        '''
        개인정보 인스턴스 제공
        :param email: User Email
        :param name: User name
        :param nickname: User ninkname
        :param phone: User Phone number
        :param password: User password
        :return:
        '''
        if not email:
            raise ValueError(_('유저는 하나의 이메일을 가지고 있어야 합니다.'))

        user = self.model(
            email=self.normalize_email(email),
            name=name,
            nickname=nickname,
            phone=phone,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, ninkname, phone, password):
        '''
        슈퍼 유저 생성
        :param email: User Email
        :param name: User name
        :param nickname: User ninkname
        :param phone: User Phone number
        :param password: User password
        :return:
        '''

        user = self.create_superuser(
            email=email,
            password=password,
            name=name,
            ninkname=ninkname,
            phone=phone,
        )

        user.is_superuser = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4())
    email = models.EmailField(
        verbose_name=_('Email Address'),
        max_length=100,
        unique=True,
    )